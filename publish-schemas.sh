#!/bin/bash

aws s3 sync --delete src/main/schemas/ s3://static.enhanced-mule.com/schemas/
aws cloudfront create-invalidation --distribution-id EYYNL6H78PMBA --paths "/*"

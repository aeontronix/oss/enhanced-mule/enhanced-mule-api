package com.aeontronix.enhancedmule.oidc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInfo {
    private String anypointBearerToken;
    private String name;
    private String username;
    private String email;
    private String anypointOrgId;
    private String anypointUserId;
    private String mavenSettingsId;

    @JsonProperty("atk")
    public String getAnypointBearerToken() {
        return anypointBearerToken;
    }

    public void setAnypointBearerToken(String anypointBearerToken) {
        this.anypointBearerToken = anypointBearerToken;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("preferred_username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("anOrgId")
    public String getAnypointOrgId() {
        return anypointOrgId;
    }

    public void setAnypointOrgId(String anypointOrgId) {
        this.anypointOrgId = anypointOrgId;
    }

    @JsonProperty("anSub")
    public String getAnypointUserId() {
        return anypointUserId;
    }

    public void setAnypointUserId(String anypointUserId) {
        this.anypointUserId = anypointUserId;
    }

    @JsonProperty("msid")
    public String getMavenSettingsId() {
        return mavenSettingsId;
    }

    public void setMavenSettingsId(String mavenSettingsId) {
        this.mavenSettingsId = mavenSettingsId;
    }
}

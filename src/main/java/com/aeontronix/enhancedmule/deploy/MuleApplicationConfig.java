/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.enhancedmule.deploy;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class MuleApplicationConfig {
    private String target;
    private final Map<String, String> properties = new HashMap<>();
    private CloudhubConfig cloudhub;
    private RTFConfig rtf;
    private Duration deployTimeout;
    private Duration deployRetryDelay;
    private Boolean mergeExistingProperties;
    private Boolean mergeExistingPropertiesOverride;
    private Boolean extMonitoring;

    public MuleApplicationConfig() {
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    @NotNull
    public CloudhubConfig getCloudhub() {
        if (cloudhub == null) {
            cloudhub = new CloudhubConfig();
        }
        return cloudhub;
    }

    public void setCloudhub(CloudhubConfig cloudhub) {
        this.cloudhub = cloudhub;
    }

    @NotNull
    public RTFConfig getRtf() {
        if (rtf == null) {
            rtf = new RTFConfig();
        }
        return rtf;
    }

    public void setRtf(RTFConfig rtf) {
        this.rtf = rtf;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public Duration getDeployTimeout() {
        return deployTimeout;
    }

    public void setDeployTimeout(Duration deployTimeout) {
        this.deployTimeout = deployTimeout;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public Duration getDeployRetryDelay() {
        return deployRetryDelay;
    }

    public void setDeployRetryDelay(Duration deployRetryDelay) {
        this.deployRetryDelay = deployRetryDelay;
    }

    public Boolean getMergeExistingProperties() {
        return mergeExistingProperties;
    }

    public void setMergeExistingProperties(Boolean mergeExistingProperties) {
        this.mergeExistingProperties = mergeExistingProperties;
    }

    public Boolean getMergeExistingPropertiesOverride() {
        return mergeExistingPropertiesOverride;
    }

    public void setMergeExistingPropertiesOverride(Boolean mergeExistingPropertiesOverride) {
        this.mergeExistingPropertiesOverride = mergeExistingPropertiesOverride;
    }

    public Boolean getExtMonitoring() {
        return extMonitoring;
    }

    public void setExtMonitoring(Boolean extMonitoring) {
        this.extMonitoring = extMonitoring;
    }

    public static MuleApplicationConfig createDefault() {
        final MuleApplicationConfig dp = new MuleApplicationConfig();
        dp.setDeployTimeout(Duration.ofMinutes(15));
        dp.setDeployRetryDelay(Duration.ofSeconds(3));
        dp.setMergeExistingProperties(false);
        dp.setMergeExistingPropertiesOverride(false);
        dp.setExtMonitoring(true);
        final CloudhubConfig ch = dp.getCloudhub();
        ch.setAppNameSuffixNPOnly(false);
        ch.setObjectStoreV1(false);
        ch.setPersistentQueues(false);
        ch.setPersistentQueuesEncrypted(false);
        ch.setCustomlog4j(false);
        ch.setStaticIPs(false);
        ch.setWorkerCount(1);
        final RTFConfig rtf = dp.getRtf();
        rtf.setCpuReserved("20m");
        rtf.setCpuLimit("1700m");
        rtf.setCpuLimit("1700m");
        rtf.setMemoryReserved("700Mi");
        rtf.setMemoryLimit("700Mi");
        rtf.setClustered(false);
        rtf.setEnforceDeployingReplicasAcrossNodes(false);
        rtf.setUpdateStrategy(RTFConfig.DeploymentModel.ROLLING);
        rtf.setReplicas(1);
        return dp;
    }
}
